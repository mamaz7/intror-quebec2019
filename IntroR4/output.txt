 Program  MARK  - Survival Rate Estimation with Capture-Recapture Data
   gfortran(GNU/Linux 64-bit) Vers. 8.   5-Feb-2018 11:16:28    Page  001
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 
   This version was compiled by GCC version 5.3.1 20160406 (Red Hat 5.3.1-6) using the options: 
     -cpp -D_REENTRANT -D IEEE -m64 -mtune=generic -march=x86-64 -O2 -fimplicit-none
     -fbounds-check -funroll-loops -ftree-vectorize
     -ffpe-summary=invalid,zero,overflow,underflow -fno-unsafe-math-optimizations
     -frounding-math -fsignaling-nans -fopenmp.

   This problem will use 3 of 4 possible threads.


  INPUT --- proc title ;

     CPU Time in seconds for last procedure was 0.00


  INPUT --- proc chmatrix occasions= 57 groups= 1 etype= 2SpecConOccup 
  INPUT --- ICMeans NoHist hist= 82 ;

  INPUT ---    time interval 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
  INPUT ---    1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1  1 1 1 1 1 1 1 1 1 1 
  INPUT ---    1 1 1 1 1 ;

  INPUT ---    glabel(1)=Group 1;

      Number of unique encounter histories read was 82.

      Number of individual covariates read was 0.
      Time interval lengths are all equal to 1.

      Data type number is 137
      Data type is Two species Conditional Occupancy Estimation                                                        

     CPU Time in seconds for last procedure was 0.01

  Program  MARK  - Survival Rate Estimation with Capture-Recapture Data
   gfortran(GNU/Linux 64-bit) Vers. 8.   5-Feb-2018 11:16:28    Page  002
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 


  INPUT --- proc estimate link=Logit NOLOOP varest=2ndPart    ;


  INPUT --- model={ 
  INPUT --- PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) 
  INPUT --- };

  INPUT ---    group=1 PsiA    rows=1 cols=1 Square ;
  INPUT ---        1 ;

  INPUT ---    group=1 PsiBA    rows=1 cols=1 Square ;
  INPUT ---        2 ;

  INPUT ---    group=1 PsiBa    rows=1 cols=1 Square ;
  INPUT ---        3 ;

  INPUT ---    group=1 pA    rows=1 cols=57 Square ;
  INPUT ---        4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 
  INPUT ---       4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 ;

  INPUT ---    group=1 pB    rows=1 cols=57 Square ;
  INPUT ---        5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
  INPUT ---       5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 ;

  INPUT ---    group=1 rA    rows=1 cols=57 Square ;
  INPUT ---        6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 
  INPUT ---       6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 ;

  INPUT ---    group=1 rBA    rows=1 cols=57 Square ;
  INPUT ---        7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 
  INPUT ---       7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 ;

  INPUT ---    group=1 rBa    rows=1 cols=57 Square ;
  INPUT ---        8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 
  INPUT ---       8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 8 ;

  INPUT ---    design matrix constraints=8 covariates=8 identity;
  INPUT ---       blabel(1)=PsiA:(Intercept);
  INPUT ---       blabel(2)=PsiBA:(Intercept);
  INPUT ---       blabel(3)=PsiBa:(Intercept);
  INPUT ---       blabel(4)=pA:(Intercept);
  INPUT ---       blabel(5)=pB:(Intercept);
  INPUT ---       blabel(6)=rA:(Intercept);
  INPUT ---       blabel(7)=rBA:(Intercept);

  Program  MARK  - Survival Rate Estimation with Capture-Recapture Data
   gfortran(GNU/Linux 64-bit) Vers. 8.   5-Feb-2018 11:16:28    Page  003
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 
  INPUT ---       blabel(8)=rBa:(Intercept);
  INPUT ---       rlabel(1)=PsiA g1 a0 t1;
  INPUT ---       rlabel(2)=PsiBA g1 a0 t1;
  INPUT ---       rlabel(3)=PsiBa g1 a0 t1;
  INPUT ---       rlabel(4)=pA g1 a0 t1;
  INPUT ---       rlabel(5)=pB g1 a0 t1;
  INPUT ---       rlabel(6)=rA g1 a0 t1;
  INPUT ---       rlabel(7)=rBA g1 a0 t1;
  INPUT ---       rlabel(8)=rBa g1 a0 t1;

 Link Function Used is LOGIT        

 Variance Estimation Procedure Used is 2ndPart 
 -2logL(saturated) = -0.0000000    
 Effective Sample Size = 100

 Number of function evaluations was 32 for 8 parameters.
 Time for numerical optimization was 0.14 seconds.
 -2logL { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) } = 4196.1717     
 Penalty { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) } = -0.0000000    
 Gradient { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) }:
  0.4856613E-04-0.1443283E-03  0.000000    -0.5160162E-04  0.000000    
 -0.1891085E-03-0.1586384E-03-0.1152721E-03
 S Vector { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) }:
   366.3632      123.8195      62.74022      28.88608      20.23149    
   14.23213     0.9017096E-05 0.1169577E-06
 Time to compute number of parameters was 0.04 seconds.
   Threshold =  0.1800000E-06     Condition index =  0.3192398E-09     New Threshold =  0.3308064E-06
 New Guessimate of Estimated Parameters { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) } = 6          
 Conditioned S Vector { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) }:
   1.000000     0.3379692     0.1712514     0.7884549E-01 0.5522251E-01
  0.3884705E-01 0.2461245E-07 0.3192398E-09
 Number of Estimated Parameters { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) } = 6          
 DEVIANCE { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) } = 4196.1717                     
 DEVIANCE Degrees of Freedom { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) } = 76            
 c-hat { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) } = 55.212786                     
 AIC { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) } = 4208.1717                  
 AICc { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) } = 4209.0750                    
 BIC { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) } = 4223.8028                    
 Pearson Chisquare { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) } = 0.5719739E+047             


 LOGIT Link Function Parameters of { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) }
                                                              95% Confidence Interval
 Parameter                    Beta         Standard Error      Lower           Upper
 -------------------------  --------------  --------------  --------------  --------------
    1:PsiA:(Intercept)      0.8726917       0.2224205       0.4367476       1.3086359     
    2:PsiBA:(Intercept)     0.8904765       0.2648259       0.3714177       1.4095353     
    3:PsiBa:(Intercept)     -21.221556      0.0000000       -21.221556      -21.221556    
    4:pA:(Intercept)        -2.5250617      0.1265960       -2.7731899      -2.2769335    
    5:pB:(Intercept)        1.2464266       0.0000000       1.2464266       1.2464266     
    6:rA:(Intercept)        -2.8475027      0.0898683       -3.0236445      -2.6713609    
    7:rBA:(Intercept)       -0.7199363      0.1860611       -1.0846159      -0.3552566    
    8:rBa:(Intercept)       -1.3669940      0.0522449       -1.4693941      -1.2645939    

  Program  MARK  - Survival Rate Estimation with Capture-Recapture Data
   gfortran(GNU/Linux 64-bit) Vers. 8.   5-Feb-2018 11:16:28    Page  004
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 


 Real Function Parameters of { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) }
                                                               95% Confidence Interval
  Parameter                  Estimate       Standard Error      Lower           Upper
 --------------------------  --------------  --------------  --------------  --------------
     1:PsiA g1 a0 t1         0.7053055       0.0462300       0.6074838       0.7872848                           
     2:PsiBA g1 a0 t1        0.7089885       0.0546399       0.5918015       0.8036926                           
     3:PsiBa g1 a0 t1        0.6075685E-009  0.0000000       0.6075685E-009  0.6075685E-009                      
     4:pA g1 a0 t1           0.0741198       0.0086878       0.0587903       0.0930514                           
     5:pB g1 a0 t1           0.7766807       0.0000000       0.7766807       0.7766807                           
     6:rA g1 a0 t1           0.0548106       0.0046557       0.0463691       0.0646846                           
     7:rBA g1 a0 t1          0.3274070       0.0409728       0.2526335       0.4121083                           
     8:rBa g1 a0 t1          0.2031059       0.0084560       0.1870347       0.2201841                           


                     Estimates of Derived Parameters
 Species Interaction Factors of { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) }
                                                95% Confidence Interval
     Group   SIF-hat        Standard Error      Lower           Upper
 ---------  --------------  --------------  --------------  --------------
       1    1.4178254       0.0929329       1.2356768       1.5999739     
 Species B Occupancy of { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) }
                                                95% Confidence Interval
     Group   PsiB-hat       Standard Error      Lower           Upper
 ---------  --------------  --------------  --------------  --------------
       1    0.5000535       0.0500054       0.4032773       0.5968257     
 Both Species Occupancy of { PsiA(~1)PsiBA(~1)PsiBa(~1)pA(~1)pB(~1)rA(~1)rBA(~1)rBa(~1) }
                                                95% Confidence Interval
     Group   PsiAB-hat      Standard Error      Lower           Upper
 ---------  --------------  --------------  --------------  --------------
       1    0.5000535       0.0500054       0.4032773       0.5968257     

 Attempted ordering of parameters by estimatibility:
  8 6 4 7 1 2 5 3
 Beta number 3 is a singular value.

     CPU Time in seconds for last procedure was 0.20

  Program  MARK  - Survival Rate Estimation with Capture-Recapture Data
   gfortran(GNU/Linux 64-bit) Vers. 8.   5-Feb-2018 11:16:28    Page  005
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 


  INPUT --- proc stop;

     CPU Time in minutes for this job was 0.00

     Time Start = 11:16:28.263   Time End = 11:16:28.352

     Wall Clock Time in minutes for this job was 0.00


          E X E C U T I O N   S U C C E S S F U L 
