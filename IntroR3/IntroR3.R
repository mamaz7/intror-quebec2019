### R code from vignette source '/home/mazerolm/Documents/Stats consulting/Formations externes/Rintro-quebec2019/IntroR3/source/IntroR-part3.rnw'
### Encoding: UTF-8

###################################################
### code chunk number 1: IntroR-part3.rnw:124-126
###################################################
options(width = 60, continue = "  ") 
setwd("/home/mazerolm/Documents/Stats consulting/Formations externes/Rintro-quebec2019/IntroR3/source")


###################################################
### code chunk number 2: importWorms
###################################################
worms <- read.table("worms.txt", header = TRUE)


###################################################
### code chunk number 3: ex55
###################################################
##creer histogramme
hist(worms$Soil.pH)


###################################################
### code chunk number 4: freqs
###################################################
freqs <- table(worms$Worm.density)
freqs


###################################################
### code chunk number 5: freqs
###################################################
freqs.alt <- with(data = worms,
                  table(Worm.density))


###################################################
### code chunk number 6: freqsAlt
###################################################
freqs.alt
freqs


###################################################
### code chunk number 7: ex56
###################################################
##creer barplot
barplot(freqs,
        xlab = "Densité de vers",
        ylab = "Fréquences")


###################################################
### code chunk number 8: ex57
###################################################
##creer barplot
boxplot(Worm.density ~ Vegetation,
        data = worms)


###################################################
### code chunk number 9: props
###################################################
props <- (1:5)/sum(1:5)
props
sum(props)


###################################################
### code chunk number 10: ex57b
###################################################
pie(props, 
    labels = 
        c("soccer", 
          "golf", 
          "rugby", 
          "football", 
          "hockey"),
    col = c("red", "blue",
            "green", "orange",
            "purple"))


###################################################
### code chunk number 11: ex58
###################################################
##creer barplot
plot(Worm.density ~ Soil.pH, 
     data = worms,
     main = "Graphique",
     cex.lab = 1.6)


###################################################
### code chunk number 12: ex57n
###################################################
##creer qqplot
qqnorm(worms$Worm.density)
##ajouter droite theorique
qqline(worms$Worm.density)


###################################################
### code chunk number 13: par (eval = FALSE)
###################################################
## ?par


###################################################
### code chunk number 14: plotworms
###################################################
plot(Worm.density ~ Soil.pH, 
     data = worms,
     ylab = "Densité",
     xlab = "pH du sol", 
     cex.lab = 1.2,
     cex.axis = 1.2)


###################################################
### code chunk number 15: plotworms2
###################################################
plot(Worm.density ~ Soil.pH, 
     data = worms,
     ylab = "Densité",
     xlab = "pH du sol", 
     cex.lab = 1.2,
     cex.axis = 1.2)

##add lines
abline(h = 3, v = 5, 
       col = c("red", 
               "blue"))


###################################################
### code chunk number 16: plotworms3 (eval = FALSE)
###################################################
## ##add lines
## abline(h = 3, v = 5, 
##        col = c("red", 
##                "blue"))


###################################################
### code chunk number 17: plotworms2b
###################################################
plot(Worm.density ~ Soil.pH, 
     data = worms,
     ylab = "Densité",
     xlab = "pH du sol", 
     cex.lab = 1.2,
     cex.axis = 1.2)

##add lines
abline(h = 3, v = 5, 
       col = c("red", 
               "blue"))

##add points
points(x = 4, y = 8, 
       pch = "+", col = 3, 
       cex = 1.5)



###################################################
### code chunk number 18: plotworms3b (eval = FALSE)
###################################################
## ##add lines
## abline(h = 3, v = 5, 
##        col = c("red", 
##                "blue"))
## ##add points
## points(x = 4, y = 8, 
##        pch = "+", col = 3, 
##        cex = 1.5)


###################################################
### code chunk number 19: plotworms2c
###################################################
plot(Worm.density ~ Soil.pH, 
     data = worms,
     ylab = "Densité",
     xlab = "pH du sol", 
     cex.lab = 1.2,
     cex.axis = 1.2)

##add lines
abline(h = 3, v = 5, 
       col = c("red", 
               "blue"))

##add points
points(x = 4, y = 8, 
       pch = "+", col = 3, 
       cex = 1.5)

##add text
text(x = 4.5, y = 6, 
     labels = "une région",
     col = "magenta", 
     cex = 2)


###################################################
### code chunk number 20: plotworms3c (eval = FALSE)
###################################################
## ##add text
## text(x = 4.5, y = 6, 
##      labels = "une région",
##      col = "magenta", 
##      cex = 2)


###################################################
### code chunk number 21: mathplot
###################################################
plot(x = 1, y = 1,
     ylab = expression(italic(Densité)),
     xlab = expression(paste("Superficie (", km^2, ")")),
     cex.lab = 1.5)

##add Greek letter with index i
mtext(side = 3,
      text = expression(psi[i]),
      line = 1, cex = 1.3)


###################################################
### code chunk number 22: ifelse
###################################################
worms$Grassland <- ifelse(worms$Vegetation == "Grassland", 1, 0)


###################################################
### code chunk number 23: meanWorms
###################################################
meanWorms <- tapply(X = worms$Worm.density, 
                    INDEX = worms$Grassland,
                    FUN = mean)
meanWorms


###################################################
### code chunk number 24: meanWorms
###################################################
sdWorms <- tapply(X = worms$Worm.density,
                  INDEX = worms$Grassland,
                  FUN = sd)
sdWorms


###################################################
### code chunk number 25: limits
###################################################
lowLim <- meanWorms - sdWorms
uppLim <- meanWorms + sdWorms


###################################################
### code chunk number 26: plotErrors1
###################################################
##valeurs x
xvals <- c(0.25, 0.75)

##points et graphique
plot(meanWorms ~ xvals, 
     type = "p",
     ylab = "Mean worm density",
     xlab = "Grassland",
     cex = 1.5,
     cex.axis = 1.5,
     cex.lab = 1.5, 
     ylim = range(c(lowLim, 
                    uppLim)),
     xlim = c(0, 1), 
     xaxt = "n")


###################################################
### code chunk number 27: plotErrors2
###################################################
##plot
plot(meanWorms ~ xvals, 
     type = "p",
     ylab = "Mean worm density",
     xlab = "Grassland",
     cex = 1.5,
     cex.axis = 1.5,
     cex.lab = 1.5, 
     ylim = range(c(lowLim, 
                    uppLim)),
     xlim = c(0, 1), 
     xaxt = "n")
##ajouter axe x
axis(side = 1, at = xvals, labels = c("no", "yes"),
     cex.axis = 1.5)


###################################################
### code chunk number 28: plotErrors2b (eval = FALSE)
###################################################
## ##ajouter axe x
## axis(side = 1, at = xvals, 
##      labels = c("no", "yes"),
##      cex.axis = 1.5)


###################################################
### code chunk number 29: plotErrors3
###################################################
##plot
plot(meanWorms ~ xvals, 
     type = "p",
     ylab = "Mean worm density",
     xlab = "Grassland",
     cex = 1.5,
     cex.axis = 1.5,
     cex.lab = 1.5, 
     ylim = range(c(lowLim, 
                    uppLim)),
     xlim = c(0, 1), 
     xaxt = "n")
##ajouter axe x
axis(side = 1, at = xvals, 
     labels = c("no", "yes"),
     cex.axis = 1.5)
##ajouter barres d'erreurs
segments(x0 = xvals, y0 = lowLim,
         x1 = xvals, y1 = uppLim)


###################################################
### code chunk number 30: plotErrors3b (eval = FALSE)
###################################################
## ##ajouter barres d'erreurs
## segments(x0 = xvals, 
##          y0 = lowLim,
##          x1 = xvals, 
##          y1 = uppLim)


###################################################
### code chunk number 31: lm
###################################################
##run regression
mod1<-lm(Worm.density~Soil.pH,
         data = worms)

##create plot
plot(Worm.density ~ Soil.pH,
     data = worms,
     ylab = "Densité de vers",
     xlab = "pH du sol",
     cex.lab = 1.2,
     cex.axis = 1.2)

##add regression line
abline(reg = mod1)


###################################################
### code chunk number 32: orderWorms
###################################################
wormsOrd <- worms[order(worms$Soil.pH), ]
wormsOrd[1:3, c("Worm.density", "Soil.pH")]


###################################################
### code chunk number 33: orderWorms
###################################################
preds <- predict(mod1, interval = "confidence",
                 newdata = wormsOrd)
preds[1:3, ]


###################################################
### code chunk number 34: ic95b (eval = FALSE)
###################################################
## ##add confidence bands
## lines(x = wormsOrd$Soil.pH,
##       y = preds[, "lwr"],
##       lty = "dotted", 
##       lwd = 1.5)
## lines(x = wormsOrd$Soil.pH,
##       y = preds[, "upr"],
##       lty = "dotted", 
##       lwd = 1.5)


###################################################
### code chunk number 35: lmCI
###################################################
##create plot
plot(Worm.density ~ Soil.pH,
     data = worms,
     cex.lab = 1.2,
     cex.axis = 1.2)

##add regression line
abline(reg = mod1)

##add confidence bands
lines(x = wormsOrd$Soil.pH,
      y = preds[, "lwr"],
      lty = "dotted", 
      lwd = 1.5)
lines(x = wormsOrd$Soil.pH,
      y = preds[, "upr"],
      lty = "dotted", 
      lwd = 1.5)


###################################################
### code chunk number 36: grassland
###################################################
worms$Grassland


###################################################
### code chunk number 37: twoSets
###################################################
grass <- worms[worms$Grassland == 1, ]
nograss <- worms[worms$Grassland == 0, ]


###################################################
### code chunk number 38: plotPoints
###################################################
##limits
lims<-range(worms$Worm.density)
##plot grassland == 1
plot(Worm.density ~ Soil.pH, 
     data = grass, 
     pch = 1, 
     col = "red", 
     ylim = lims, cex = 1.5,
     cex.lab = 1.5, 
     cex.axis = 1.5)


###################################################
### code chunk number 39: plotPoints2 (eval = FALSE)
###################################################
## ##grassland == 0
## points(Worm.density ~ Soil.pH, 
##        data = nograss, 
##        pch = 2, 
##        col = "purple",
##        cex = 1.5)


###################################################
### code chunk number 40: plotPoints2b
###################################################
##plot
plot(Worm.density ~ Soil.pH, 
     data = grass, 
     pch = 1, 
     col = "red", 
     ylim = lims, cex = 1.5,
     cex.lab = 1.5,
     cex.axis = 1.5)
points(Worm.density ~ Soil.pH, 
       data = nograss, 
       pch = 2, 
       col = "purple", 
       cex = 1.5)


###################################################
### code chunk number 41: plotPoints3 (eval = FALSE)
###################################################
## ##add legend
## legend(x = "topleft", 
##        legend = c("grass", 
##                   "nograss"),
##        pch = c(1, 2),
##        col = c("red", 
##                "purple"),
##        bty = "n",
##        title = "Vegetation",
##        cex = 1.5)


###################################################
### code chunk number 42: plotPoints3b
###################################################
##plot
plot(Worm.density ~ Soil.pH, 
     data = grass, 
     pch = 1, 
     col = "red", 
     ylim = lims, cex = 1.5,
     cex.lab = 1.5, 
     cex.axis = 1.5)
points(Worm.density ~ Soil.pH, 
       data = nograss, 
       pch = 2, 
       col = "purple", 
       cex = 1.5)
##add legend
legend(x = "topleft", 
       legend = c("grass", 
                  "nograss"),
       pch = c(1, 2),
       col = c("red", 
               "purple"),
       bty = "n",
       title = "Vegetation",
       cex = 1.5)


###################################################
### code chunk number 43: colors1 (eval = FALSE)
###################################################
## colors( )


###################################################
### code chunk number 44: colors2
###################################################
txt <- capture.output({
colors( )
})
if(length(txt) > 4 ){
    txt <- c(txt[1:5], "...", txt[324:329])
}
cat(txt, sep = "\n" )


###################################################
### code chunk number 45: rgb
###################################################
couleur <- rgb(red = 90,
               green = 178,
               blue = 13,
               maxColorValue = 255)
couleur
plot(Worm.density ~ Soil.pH,
     data = worms,
     col = couleur,
     cex.lab = 1.5,
     cex.axis = 1.5,
     cex = 1.5)


###################################################
### code chunk number 46: mfrow (eval = FALSE)
###################################################
## par(mfrow = c(rangées, colonnes))


###################################################
### code chunk number 47: mfcol (eval = FALSE)
###################################################
## par(mfcol = c(rangées, colonnes))


###################################################
### code chunk number 48: mfrow2
###################################################
par(mfrow = c(2, 2))
plot(0:5, 0:5, type = "n")
text(x = 1, y = 1,
     labels = "A",
     cex = 2)
plot(0:5, 0:5, type = "n")
text(x = 1, y = 1,
     labels = "B",
     cex = 2)
plot(0:5, 0:5, type = "n")
text(x = 1, y = 1,
     labels = "C",
     cex = 2)
plot(0:5, 0:5, type = "n")
text(x = 1, y = 1,
     labels = "D",
     cex = 2)
#add mtext
mtext(side = 3, line = -2, outer = TRUE, 
      text = "par(mfrow = c(2, 2))", 
      cex = 2.5)


###################################################
### code chunk number 49: mfcol2
###################################################
par(mfcol = c(2, 2))
plot(0:5, 0:5, type = "n")
text(x = 1, y = 1,
     labels = "A",
     cex = 2)
plot(0:5, 0:5, type = "n")
text(x = 1, y = 1,
     labels = "B",
     cex = 2)
plot(0:5, 0:5, type = "n")
text(x = 1, y = 1,
     labels = "C",
     cex = 2)
plot(0:5, 0:5, type = "n")
text(x = 1, y = 1,
     labels = "D",
     cex = 2)
#add mtext
mtext(side = 3, line = -2, outer = TRUE, 
      text = "par(mfcol = c(2, 2))", 
      cex = 2.5)


###################################################
### code chunk number 50: pdf (eval = FALSE)
###################################################
## pdf(file = "Fig1.pdf")
## plot(x = c(1, 2), y = c(6, 5))
## dev.off( )


###################################################
### code chunk number 51: tiff (eval = FALSE)
###################################################
## tiff(file = "Fig1.tiff")
## plot(x = c(1, 2), y = c(6, 5))
## dev.off( )


