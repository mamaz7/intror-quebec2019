---
title: "Formation d'introduction au langage et environnement R"
author: "Marc J. Mazerolle"
date: '2019-05-01'
---

<br/>

# Introduction au langage et environnement ```R```
**Date:** 13 et 14 mai 2019

**Heure:** 8h30 -- 17h00

**Lieu:** salle 2320, Pavillon Desjardins, Université Laval, Québec

**Important:** Vous devez apporter un ordinateur portatif pour la formation avec les logiciels ```R``` et ```R Studio Desktop``` préalablement installés. Vous trouverez la marche à suivre pour l'installation des deux logiciels dans le document [InstallationR.pdf](https://gitlab.com/mamaz7/intror-quebec2019/blob/master/InstallationR.pdf).

<br/>

## Matériel à télécharger pour la formation
Vous trouverez ci-dessous tous les fichiers associés à chaque partie de la formation. Les fichiers sont compressés dans une archive ```zip```, que vous pourrez décompresser à l'aide d'un logiciel comme [7-zip](https://www.7-zip.org/).


Pour chaque partie de la formation, l'archive ```zip``` inclut le matériel suivant:

* présentations en format ```pdf``` (couleur ou noir et blanc)
* scripts ```R``` du contenu des présentations
* jeux de données
* exercices
* solutionnaire des exercices (```pdf``` et scripts ```R```)


Télécharger le matériel

* Partie 1: [IntroR1.zip](https://gitlab.com/mamaz7/intror-quebec2019/blob/master/IntroR1/IntroR1.zip)
* Partie 2: [IntroR2.zip](https://gitlab.com/mamaz7/intror-quebec2019/blob/master/IntroR2/IntroR2.zip)
* Partie 3: [IntroR3.zip](https://gitlab.com/mamaz7/intror-quebec2019/blob/master/IntroR3/IntroR3.zip)
* Partie 4: [IntroR4.zip](https://gitlab.com/mamaz7/intror-quebec2019/blob/master/IntroR4/IntroR4.zip)
